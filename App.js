import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider as ReduxProvider } from 'react-redux';
import SessionProvider from './src/providers/session';
import ThemeProvider from './src/providers/theme';
import Router from './src/router';
import store from './src/redux';

export default function App() {
  return (
    <ReduxProvider store={store}>
      <SessionProvider>
        <ThemeProvider>
          <SafeAreaProvider>
            <Router />
          </SafeAreaProvider>
        </ThemeProvider>
      </SessionProvider>
    </ReduxProvider>
  );
}