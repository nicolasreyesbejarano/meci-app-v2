module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          root: ['./'],
          alias: {
            '@components': './src/components',
            '@containers': './src/containers',
            '@redux': './src/redux',
            '@sagas': './src/redux/sagas',
            '@ducks': './src/redux/ducks',
            '@thunks': './src/redux/thunks',
            '@utils': './src/utils',
            '@services': './src/services',
            '@screens': './src/screens',
            '@images': './assets/images',
            '@router': './src/router',
          },
        },
      ],
    ],
  };
};
