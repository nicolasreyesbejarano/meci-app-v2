import styled from 'styled-components/native';

export const SearchContainer = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.secondBackground};
  padding-top: 0;
  padding: 25px;
`;
export const SearchContent = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.white01};
  padding-right: 25px;
  border-radius: 25px;
`;
export const SearchContentBody = styled.View`
  flex-grow: 1;
  position: relative;
  margin-top: 20px;
  justify-content: center;
  align-items: center;
`;
export const TitleDisabled = styled.Text`
  font-size: 20px;
  margin-top: 10px;
  font-weight: bold;
  text-align: justify;
  color: ${(props) => props.theme.gray02};
`;
export const TextFound = styled.Text`
  font-size: 16px;
  font-weight: bold;
  text-align: justify;
  color: ${(props) => props.theme.gray01};
`;
export const TextFoundContent = styled.TouchableHighlight.attrs((props) => ({
  underlayColor: props.theme.gray02,
}))`
  width: 100%;
  border-bottom-width: 0.3px;
  padding-bottom: 5px;
  margin-bottom: 15px;
  border-color: ${(props) => props.theme.gray02};
`;
export const SearchContentFound = styled.View`
  width: 100%;
  border-radius: 20px;
  position: absolute;
  background-color: ${(props) => props.theme.background};
  justify-content: flex-start;
  align-items: flex-start;
  top: 0;
  min-height: 100px;
  padding: 20px;
`;
