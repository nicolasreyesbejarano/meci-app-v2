import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { ActivityIndicator } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import Input from '../../components/Input';
import { requestSearch } from '../../redux/ducks/search.duck';
import { setCategory } from '../../redux/ducks/category.duck';
import {
  SearchContainer,
  SearchContent,
  SearchContentFound,
  SearchContentBody,
  TitleDisabled,
  TextFoundContent,
  TextFound,
} from './styles';

const SearchBar = () => {
  const navigation = useNavigation();
  const [value, setValue] = useState('');
  const [focus, setFocus] = useState(false);
  const dispatch = useDispatch();
  const { fetching, data } = useSelector((state) => state.search);
  const handleCategory = (information) => {
    dispatch(setCategory(information));
    setTimeout(() => {
      navigation.navigate('CategoriesRender');
    }, 100);
  };
  console.log("DIABLOS", data)
  return (
    <SearchContainer>
      <SearchContent>
        <Input
          placeholder="Buscar..."
          onChangeText={(value) => {
            dispatch(requestSearch(value.toLowerCase()));
            setValue(value.toLowerCase());
          }}
          onBlur={() => {
            setFocus(false);
            setValue('');
          }}
          onFocus={() => setFocus(true)}
          value={value}
        />
        {fetching ? (
          <ActivityIndicator size="small" color="#07a8ca" />
        ) : (
          <Feather name="search" size={25} color="#07a8ca" />
        )}
      </SearchContent>
      <SearchContentBody>
        {focus && data && data.length !== 0 && (
          <SearchContentFound style={{ elevation: 3, zIndex: 99 }}>
            {data?.map((section) => (
              <TextFoundContent
                key={section.Seccion}
                onPress={() => handleCategory(section)}
              >
                <TextFound>{section.nombre}</TextFound>
              </TextFoundContent>
            ))}
          </SearchContentFound>
        )}
        <Feather name="search" size={80} color="#e0e0e0" />
        <TitleDisabled>Encuentra Categorías</TitleDisabled>
      </SearchContentBody>
    </SearchContainer>
  );
};

export default SearchBar;
