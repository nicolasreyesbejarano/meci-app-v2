import React, { useState, useContext } from 'react';
import { View } from 'react-native';
import * as Crypto from 'expo-crypto';
import serviceManager from '../../utils/servicesManager';
import { regexValidateEmail } from '../../utils/utils';
import { useDispatch, useSelector } from 'react-redux';
import { loginRequest } from '../../redux/thunks/auth.thunk';
import { SessionContext } from '../../providers/session';
import Input from '../../components/Input';
import ButtonGeneric from '../../components/ButtonGeneric';
import { Text, ErrorContainer } from './styles';

const LoginForm = () => {
  const { createSession } = useContext(SessionContext);
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [errorInput, setErrorInput] = useState(null);
  const dispatch = useDispatch();
  const { fetching } = useSelector((state) => state.login);
  const handleChange = async () => {
    if (fetching) return;
    setErrorInput(null);
    if (email !== '' && password !== '') {
      if (regexValidateEmail(email)) {
        const passwordMD5 = await Crypto.digestStringAsync(
          Crypto.CryptoDigestAlgorithm.MD5,
          password
        );
        const promise = loginRequest(dispatch, {
          user: email,
          pwd: passwordMD5,
        });
        promise
          .then((response) => {
            if (
              !response.hasOwnProperty('error') &&
              response.hasOwnProperty('access_token')
            ) {
              serviceManager.setAuthorization(response.access_token);
              createSession(response.access_token, email);
            } else {
              setErrorInput(response.error || 'Credenciales invalidas');
              setPassword('');
            }
          })
          .catch((error) => {
            setErrorInput('Credenciales invalidas');
          });
      } else {
        setErrorInput('Por favor ingrese un correo valido');
      }
    } else {
      setErrorInput('Por favor diligencie todos los campos');
    }
  };
  return (
    <View>
      <Input
        style={{ marginBottom: 20 }}
        placeholder="Correo"
        onChangeText={setEmail}
        value={email}
      />
      <Input
        style={{ marginBottom: 50 }}
        placeholder="Contraseña"
        secureTextEntry={true}
        onChangeText={setPassword}
        value={password}
      />
      <ButtonGeneric
        title="Iniciar sesión"
        invert
        loading={fetching}
        disabled={fetching}
        onPress={handleChange}
      />
      {errorInput !== null && (
        <ErrorContainer>
          <Text>{errorInput}</Text>
        </ErrorContainer>
      )}
    </View>
  );
};

export default LoginForm;
