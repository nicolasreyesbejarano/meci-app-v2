import React, { useContext, useEffect } from 'react';
import { View, ActivityIndicator } from 'react-native';
import Input from '../../components/Input';
import ButtonGeneric from '../../components/ButtonGeneric';
import { SessionContext } from '../../providers/session';
import { Label, LoadingContainer, SubTitle } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { requestUserData } from '../../redux/ducks/userData.duck';

const UserData = () => {
  const dispatch = useDispatch();
  const { deleteSession, getUserID } = useContext(SessionContext);
  const { fetching, data } = useSelector((state) => state.userData);
  useEffect(() => {
    asyncEffect();
  }, []);
  const asyncEffect = async () => {
    const email = await getUserID();
    dispatch(requestUserData(email));
  };
  if (fetching)
    return (
      <LoadingContainer>
        <ActivityIndicator size="small" color={'#07a8ca'} />
        <SubTitle>Cargando informacion...</SubTitle>
      </LoadingContainer>
    );
  return (
    <View style={{ flex: 1 }}>
      <Label>Activo:</Label>
      <Input
        style={{ marginBottom: 10 }}
        placeholder="Correo"
        editable={false}
        value={data.activo ? 'Si' : 'No'}
      />
      <Label>Nombre:</Label>
      <Input
        style={{ marginBottom: 10 }}
        placeholder="Contraseña"
        editable={false}
        value={data.nombreUsuario}
      />
      <Label>Email:</Label>
      <Input
        style={{ marginBottom: 10 }}
        placeholder="Contraseña"
        editable={false}
        value={data.usuario}
      />
      <Label>Fecha de Registro:</Label>
      <Input
        style={{ marginBottom: 30 }}
        placeholder="Contraseña"
        editable={false}
        value={data.fechaRegistro}
      />
      <View
        style={{
          flexGrow: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <ButtonGeneric title="Cerrar sesión" invert onPress={deleteSession} />
      </View>
    </View>
  );
};

export default UserData;
