import styled from 'styled-components/native';

export const Label = styled.Text`
  font-size: 12px;
  font-weight: bold;
  color: ${(props) => props.theme.gray01};
  margin-bottom: 10px;
  padding-left: 20px;
`;
export const SubTitle = styled.Text`
  font-size: 20px;
  margin-bottom: 20px;
  font-weight: bold;
  text-align: justify;
  color: ${(props) => props.theme.gray01};
`;
export const LoadingContainer = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
