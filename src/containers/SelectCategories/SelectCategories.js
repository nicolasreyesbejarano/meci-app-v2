import React, { useState, useEffect } from 'react';
import { View, FlatList, Dimensions } from 'react-native';
import { setCategory } from '../../redux/ducks/category.duck';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import ChannelCard from "../../components/ChannelCard";

const DEFAULT = [
  { label: 'Error en el servicio', value: 'error', section: { color:['red']} },
];
const SelectCategories = (props) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [section, setSection] = useState({});
  const [dataToSend, setDataToSend] = useState(DEFAULT);
  const { dataSelected } = useSelector((state) => state.home);
  let height = 60;
  let size = { height: `${height}px`, width: `100%` };
  useEffect(() => {
    const response = handleData();
    setDataToSend(response);
  }, []);
  const goToExplore = (section) => {
    dispatch(setCategory(section));
    setTimeout(() => {
      navigation.navigate('CategoriesRender');
    }, 100);
  };
  const handleData = () => {
    let data = DEFAULT;

    if (dataSelected.hasOwnProperty('subcategorias')) {
      data = dataSelected.subcategorias.map((value, index) => ({
        label: value.nombre,
        value: value.Seccion,
        index: index,
        section: value,
      }));
    }
    return data;
  };
  const renderItem = ({ item }) => {
    return (
      <ChannelCard
        {...size}
        title={item.label}
        color={item.section.color[0]}
        onPress={() => goToExplore(item.section)}
      />
    );
  };
  return (
      <FlatList
        ListHeaderComponent={props.Header}
        contentContainerStyle={{
          paddingVertical: 20,
        }}
        style={{
          paddingHorizontal:15
        }}
        data={dataToSend}
        ItemSeparatorComponent={({ highlighted }) => (
          <View style={{ marginBottom: 10 }} />
        )}
        renderItem={renderItem}
        keyExtractor={(item) => item.value}
      />
  );
};

export default SelectCategories;
