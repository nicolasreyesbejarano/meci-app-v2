import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { regexValidateEmail } from '../../utils/utils';
import { recoverRequest } from '../../redux/thunks/auth.thunk';
import { View } from 'react-native';
import Input from '../../components/Input';
import ButtonGeneric from '../../components/ButtonGeneric';
import { Text, ErrorContainer } from './styles';

const RecoverPasswordForm = ({ onSuccess }) => {
  const [email, setEmail] = useState('');
  const [errorInput, setErrorInput] = useState(null);
  const dispatch = useDispatch();
  const { fetching } = useSelector((state) => state.recover);
  const handleChange = async () => {
    if (fetching) return;
    setErrorInput(null);
    if (email !== '') {
      if (regexValidateEmail(email)) {
        const promise = recoverRequest(dispatch, email);
        promise
          .then((response) => {
            if (response) {
              setEmail('');
              setErrorInput(null);
              onSuccess();
            } else {
              setErrorInput('El Correo es invalido');
            }
          })
          .catch((error) => {
            setErrorInput(error);
          });
      } else {
        setErrorInput('Por favor ingrese un correo valido');
      }
    } else {
      setErrorInput('Por favor diligencie todos los campos');
    }
  };
  return (
    <View>
      <Input
        style={{ marginBottom: 40 }}
        placeholder="Correo"
        onChangeText={setEmail}
        value={email}
      />
      <ButtonGeneric
        title="Enviar"
        invert
        loading={fetching}
        disabled={fetching}
        onPress={handleChange}
      />
      {errorInput !== null && (
        <ErrorContainer>
          <Text>{errorInput}</Text>
        </ErrorContainer>
      )}
    </View>
  );
};

export default RecoverPasswordForm;
