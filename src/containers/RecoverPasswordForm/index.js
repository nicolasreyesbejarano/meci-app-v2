import RecoverPasswordForm from './RecoverPasswordForm';

RecoverPasswordForm.defaultProps = {
  onSuccess: () => {},
};

export default RecoverPasswordForm;
