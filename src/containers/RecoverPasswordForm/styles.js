import styled from 'styled-components/native';

export const Text = styled.Text`
  margin-top: 60px;
  font-size: 16px;
  text-align: justify;
  color: ${(props) => props.theme.red01};
`;

export const ErrorContainer = styled.View`
  width: 100%;
  text-align: center;
  align-items: center;
`;
