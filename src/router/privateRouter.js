import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import DrawerNavigation from './DrawerNavigation';
import TabNavigation from './TabNavigation';
import Categories from '../screens/Categories';
const Stack = createStackNavigator();

const PrivateRoutes = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="TabNavigation" component={TabNavigation} />
      <Stack.Screen name="CategoriesRender" component={Categories} />
    </Stack.Navigator>
  );
};

export default PrivateRoutes;
