import React, { useContext } from 'react';
import { StatusBar } from 'react-native';
import { SessionContext } from '../providers/session';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import PrivateRoutes from './privateRouter';
import PublicRoutes from './publicRouter';

const Router = () => {
  const { session } = useContext(SessionContext);
  return (
    <NavigationContainer
      theme={{
        ...DefaultTheme,
        colors: { ...DefaultTheme.colors, background: '#FFF' },
      }}
    >
      <StatusBar barStyle="light-content" />
      {session ? <PrivateRoutes /> : <PublicRoutes />}
    </NavigationContainer>
  );
};

export default Router;
