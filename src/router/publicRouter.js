import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/Login';
import Manual from '../screens/Manual';
import OnBoarding from '../screens/OnBoarding';
import RecoverPassword from '../screens/RecoverPassword';

const Stack = createStackNavigator();

const PublicRoutes = () => {
  return (
    <Stack.Navigator
      headerMode="none"
      screenOptions={{
        headerTitleAlign: 'center',
      }}
    >
      <Stack.Screen name="On Boarding" component={OnBoarding} />
      <Stack.Screen name="Manual" component={Manual} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="RecoverPassword" component={RecoverPassword} />
    </Stack.Navigator>
  );
};

export default PublicRoutes;
