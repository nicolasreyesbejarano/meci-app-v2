import React from 'react';
import Home from '../screens/Home/router';
import Profile from '../screens/Profile';
import Search from '../screens/Search';
import { SimpleLineIcons, Feather } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

const TabNavigation = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        pressColor: '#FFF',
        inactiveTintColor: '#3e3e3e',
        activeBackgroundColor: '#FFF',
        activeTintColor: '#07a8ca',
        tabStyle: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 10,
        },
        style: {
          paddingBottom: 0,
        },
      }}
      initialRouteName="Home"
    >
      {/* <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Feather name="search" size={size} color={color} />
          ),
          tabBarLabel: '',
        }}
      /> */}
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({ color, size }) => (
            <SimpleLineIcons name="home" size={size} color={color} />
          ),
          tabBarLabel: '',
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Feather name="user" size={size} color={color} />
          ),
          tabBarLabel: '',
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigation;
