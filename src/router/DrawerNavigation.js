import React from 'react';
import TabNavigation from './TabNavigation';
import Categories from '../screens/Categories';
import Channels from '../screens/Home/Channels';
import CategoriesHome from '../screens/Home/Categories';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator initialRouteName="Home page">
      <Drawer.Screen
        name="MN"
        component={TabNavigation}
        options={{ drawerLabel: 'Menú' }}
      />
      <Drawer.Screen
        name="DSC"
        component={() => (
          <Stack.Navigator>
            <Stack.Screen name="ChannelsDescription" component={Channels} />
          </Stack.Navigator>
        )}
        options={{ drawerLabel: 'Descripción de los Canales' }}
      />
      <Drawer.Screen
        name="CG"
        component={() => <Categories screenProps="conceptosGenerales" />}
        options={{ drawerLabel: 'Conceptos Generales' }}
      />
      <Drawer.Screen
        name="CHV"
        component={() => (
          <Stack.Navigator>
            <Stack.Screen name="CategoriesHome" component={CategoriesHome} />
          </Stack.Navigator>
        )}
        options={{ drawerLabel: 'Categorías HV' }}
      />
      <Drawer.Screen
        name="CONTC"
        component={() => <Categories screenProps="CanalesContacto" />}
        options={{ drawerLabel: 'Contacto' }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
