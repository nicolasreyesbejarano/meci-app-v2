export default function (axios) {
  return {
    login: ({ user, pwd }) => {
      let formData = new FormData();
      formData.append('user', user);
      formData.append('pwd', pwd);
      return axios.post('login', formData);
    },
    recover: (user) => {
      let formData = new FormData();
      formData.append('user', user);
      return axios.post('recover', formData);
    },
    getUserData: (email) => {
      return axios.get(`getInfoUser?usuario=${email}`);
    },
  };
}
