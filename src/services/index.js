import servicesManager from '../utils/servicesManager';
import Auth from './auth.service';
import Category from './categories.service';

const meciAxios = servicesManager.api();

export const auth = new Auth(meciAxios);
export const category = new Category(meciAxios);
