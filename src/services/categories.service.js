export default function (axios) {
  return {
    search: (input = '') => {
      return axios.get(`search?criteria=${input}`);
    },
    get: () => {
      return axios.get('categories');
    },
  };
}
