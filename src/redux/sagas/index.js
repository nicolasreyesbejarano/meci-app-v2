import { all } from 'redux-saga/effects';
import watchSearchAsync from './categories.saga';
import watchAuthAsync from './userData.saga';

export default function* rootSaga() {
  yield all([watchSearchAsync(), watchAuthAsync()]);
}
