import { put, takeLatest, call, delay } from 'redux-saga/effects';
import { category } from '../../services';
import {
  requestSearch,
  successSearch,
  failureSearch,
} from '../ducks/search.duck';
import { requestHome, successHome, failureHome } from '../ducks/home.duck';

function* search({ payload }) {
  try {
    yield delay(2000);
    if (payload.input !== '') {
      const response = yield call(category.search, payload.input);
      yield put(successSearch(response.data.results));
    }
  } catch (error) {
    yield put(failureSearch(error));
  }
}

function* getCategories() {
  try {
    const response = yield call(category.get);
    yield put(successHome(response.data));
  } catch (error) {
    yield put(failureHome(error));
  }
}

function* watchSearchAsync() {
  yield takeLatest(requestSearch, search);
  yield takeLatest(requestHome, getCategories);
}

export default watchSearchAsync;
