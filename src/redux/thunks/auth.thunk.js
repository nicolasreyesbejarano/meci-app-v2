import { auth } from '../../services';
import { requestLogin, successLogin, failureLogin } from '../ducks/login.duck';
import {
  requestRecover,
  successRecover,
  failureRecover,
} from '../ducks/recover.duck';

export const loginRequest = (dispatch, userData) => {
  dispatch(requestLogin());
  return auth
    .login(userData)
    .then((response) => {
      dispatch(successLogin());
      return response.data;
    })
    .catch((error) => {
      dispatch(failureLogin(error));
      throw error;
    });
};

export const recoverRequest = (dispatch, user) => {
  dispatch(requestRecover());
  return auth
    .recover(user)
    .then((response) => {
      dispatch(successRecover());
      return response.data;
    })
    .catch((error) => {
      dispatch(failureRecover(error));
      throw error;
    });
};
