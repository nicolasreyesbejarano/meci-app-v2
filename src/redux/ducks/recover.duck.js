import { createActions, handleActions } from 'redux-actions';

const defaultState = {
  fetching: false,
  message: '',
  error: false,
  success: false,
};

export const { requestRecover, successRecover, failureRecover } = createActions(
  {
    REQUEST_RECOVER: () => ({
      ...defaultState,
      fetching: true,
    }),
    SUCCESS_RECOVER: () => ({
      fetching: false,
      success: true,
    }),
    FAILURE_RECOVER: (message) => ({
      fetching: false,
      error: true,
      message,
    }),
  }
);

const recover = handleActions(
  {
    [requestRecover]: (
      draft,
      { payload: { fetching, message, error, success } }
    ) => {
      return {
        ...draft,
        fetching,
        message,
        error,
        success,
      };
    },
    [successRecover]: (draft, { payload: { fetching, success } }) => ({
      ...draft,
      fetching,
      success,
    }),
    [failureRecover]: (draft, { payload: { fetching, error, message } }) => ({
      ...draft,
      fetching,
      error,
      message,
    }),
  },
  defaultState
);

export default recover;
