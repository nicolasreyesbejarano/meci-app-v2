import { combineReducers } from 'redux';
import home from './home.duck';
import login from './login.duck';
import recover from './recover.duck';
import search from './search.duck';
import category from './category.duck';
import userData from './userData.duck';

const reducers = combineReducers({
  home,
  login,
  search,
  recover,
  userData,
  category,
});

export default reducers;
