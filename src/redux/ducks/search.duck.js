import { createActions, handleActions } from 'redux-actions';

const defaultState = {
  fetching: false,
  message: '',
  error: false,
  success: false,
  data: [],
};

export const { requestSearch, successSearch, failureSearch } = createActions({
  REQUEST_SEARCH: (input) => ({
    ...defaultState,
    fetching: true,
    input,
  }),
  SUCCESS_SEARCH: (data) => ({
    fetching: false,
    success: true,
    data,
  }),
  FAILURE_SEARCH: (message) => ({
    fetching: false,
    error: true,
    message,
  }),
});

const search = handleActions(
  {
    [requestSearch]: (
      draft,
      { payload: { fetching, message, error, success, data } }
    ) => {
      return {
        ...draft,
        fetching,
        message,
        error,
        success,
        data,
      };
    },
    [successSearch]: (draft, { payload: { fetching, success, data } }) => ({
      ...draft,
      fetching,
      success,
      data,
    }),
    [failureSearch]: (draft, { payload: { fetching, error, message } }) => ({
      ...draft,
      fetching,
      error,
      message,
    }),
  },
  defaultState
);

export default search;
