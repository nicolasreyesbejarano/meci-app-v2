import { createActions, handleActions } from 'redux-actions';

const defaultState = {
  data: {},
};

export const { setCategory, removeCategory } = createActions({
  SET_CATEGORY: (data) => ({
    data,
  }),
  REMOVE_CATEGORY: () => ({}),
});

const category = handleActions(
  {
    [setCategory]: (draft, { payload: { data } }) => {
      return {
        ...draft,
        data,
      };
    },
    [removeCategory]: (draft) => ({
      ...draft,
      data: defaultState.data,
    }),
  },
  defaultState
);

export default category;
