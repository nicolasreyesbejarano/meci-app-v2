import { createActions, handleActions } from 'redux-actions';

const defaultState = {
  fetching: false,
  message: '',
  error: false,
  success: false,
  data: [],
  dataSelected: {},
};

export const {
  requestHome,
  successHome,
  failureHome,
  selectHome,
} = createActions({
  REQUEST_HOME: () => ({
    ...defaultState,
    fetching: true,
  }),
  SUCCESS_HOME: (data) => ({
    fetching: false,
    success: true,
    data,
  }),
  FAILURE_HOME: (message) => ({
    fetching: false,
    error: true,
    message,
  }),
  SELECT_HOME: (selected) => ({
    selected,
  }),
});

const home = handleActions(
  {
    [requestHome]: (
      draft,
      { payload: { fetching, message, error, success, data } }
    ) => {
      return {
        ...draft,
        fetching,
        message,
        error,
        success,
        data,
      };
    },
    [successHome]: (draft, { payload: { fetching, success, data } }) => ({
      ...draft,
      fetching,
      success,
      data,
    }),
    [failureHome]: (draft, { payload: { fetching, error, message } }) => ({
      ...draft,
      fetching,
      error,
      message,
    }),
    [selectHome]: (draft, { payload: { selected } }) => ({
      ...draft,
      dataSelected: selected,
    }),
  },
  defaultState
);

export default home;
