const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const numbersRegex = /^[0-9]+$/;
const alphanumericRegex = /^[a-zA-Z0-9]*$/;

export const regexValidateEmail = (email) => {
  return emailRegex.test(email);
};
