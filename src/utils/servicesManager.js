import axios from 'axios';
const BASE_URL = 'http://54.71.36.162:3001/';
const CONTENT_TYPE = 'application/json';

class ServicesManager {
  constructor() {
    this.axiosDefault = axios.create();
    this.init();
  }

  init() {
    this.setHeader();
    this.handleError();
    this.setAuthorization();
  }
  api(url = BASE_URL) {
    this.setBaseUrl(url);
    return this.axiosDefault;
  }
  create(properties = {}) {
    return axios.create(properties);
  }
  setBaseUrl(url = BASE_URL) {
    this.axiosDefault.defaults.baseURL = url;
  }

  setHeader(contentType = CONTENT_TYPE) {
    this.axiosDefault.defaults.headers.post['Content-Type'] = contentType;
  }
  setAuthorization(token) {
    if (token) {
      this.axiosDefault.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${token}`;
    }
  }

  handleError() {
    this.axiosDefault.interceptors.response.use(
      function (response) {
        return response;
      },
      function (error) {
        return Promise.reject(error.message);
      }
    );
  }
  deleteInterceptor(interceptor) {
    axios.interceptors.request.eject(interceptor);
  }
}

export default new ServicesManager();
