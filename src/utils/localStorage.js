import { AsyncStorage } from 'react-native';

export const getStorage = (key) => {
  const response = AsyncStorage.getItem(key.toString());
  if (!response) return false;
  return response;
};

export const setStorage = (key, value) => {
  let dataToSave = value;
  if (typeof dataToSave == 'object') dataToSave = JSON.stringify(dataToSave);
  const response = AsyncStorage.setItem(key, dataToSave);
  if (!response) return false;
  return response;
};

export const deleteStorage = (key) => {
  AsyncStorage.removeItem(key.toString());
  const response = AsyncStorage.getItem(key.toString());
  if (!response) return false;
  return true;
};

export const clearStorage = () => {
  AsyncStorage.clear();
  return true;
};
