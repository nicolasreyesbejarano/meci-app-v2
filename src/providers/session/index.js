import React, { useState, useLayoutEffect } from 'react';
import serviceManager from '../../utils/servicesManager';
import {
  getStorage,
  setStorage,
  deleteStorage,
} from '../../utils/localStorage';

export const SessionContext = React.createContext({
  session: false,
  deleteSession: () => {},
  createSession: () => {},
  getSession: () => {},
  getUserID: () => {},
});

function SessionProvider({ children }) {
  const [session, setSession] = useState(false);

  useLayoutEffect(() => {
    //deleteSession();
    getStorage('user-session-meci').then((response) => {
      if (response) {
        let token = JSON.parse(response);
        setSession(true);
        serviceManager.setAuthorization(token);
      }
    });
  }, [session]);

  const deleteSession = () => {
    deleteStorage('user-session-meci');
    setSession(false);
  };

  const createSession = (session, email) => {
    setStorage('user-session-meci', JSON.stringify(session));
    saveUserID(email);
    setSession(true);
  };
  const saveUserID = (email) => {
    setStorage('user-email-meci', JSON.stringify(email));
  };
  const getUserID = async () => {
    let userSession = await getStorage('user-email-meci');
    let email = JSON.parse(userSession);
    return email;
  };
  const getSession = async () => {
    let userSession = await getStorage('user-session-meci');
    return userSession;
  };
  return (
    <SessionContext.Provider
      value={{ session, createSession, deleteSession, getSession, getUserID }}
    >
      {children}
    </SessionContext.Provider>
  );
}

export default SessionProvider;
