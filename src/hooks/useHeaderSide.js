import React, { useLayoutEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Image, Dimensions } from 'react-native';
import imageHV from '../../logo-hv.png';

const useHeaderSide = () => {
  const navigation = useNavigation();
  const height = Dimensions.get('window').height * 0.15;
  useLayoutEffect(() => {
    navigation.setOptions({
      title: '',
      headerStyle: {
        height: height,
      },
      headerBackground: () => (
        <View
          style={{
            borderBottomLeftRadius: border,
            borderBottomRightRadius: border,
            flexGrow: 1,
            overflow: 'hidden',
          }}
        >
          <Image
            source={imageHV}
            style={{
              flex: 1,
              width: 180,
              height: null,
              resizeMode: 'contain',
            }}
          />
        </View>
      ),
    });
  }, [navigation]);
};

export default useHeaderSide;
