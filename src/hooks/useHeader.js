import React, { useLayoutEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { View, Image, Dimensions } from 'react-native';
import imageHV from '../../assets/images/logo-hv.png';

const useHeader = ({ border, back = true }) => {
  const navigation = useNavigation();
  const height = Dimensions.get('window').height * 0.15;
  useLayoutEffect(() => {
    navigation.setOptions({
      title: '',
      headerStyle: {
        height: height,
      },
      headerLeft: back
        ? () => (
            <Ionicons
              name="ios-arrow-back"
              size={35}
              color="#FFF"
              onPress={() => navigation.goBack()}
              style={{ paddingVertical: 0, paddingHorizontal: 10, marginLeft: 10 }}
            />
          )
        : null,
      headerBackground: () => (
        <View
          style={{
            borderBottomLeftRadius: border,
            borderBottomRightRadius: border,
            flexGrow: 1,
            overflow: 'hidden',
          }}
        >
          <LinearGradient
            style={{
              flex: 1,
              borderBottomLeftRadius: border,
              borderBottomRightRadius: border,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'relative',
            }}
            colors={['#0081ab', '#5ca664']}
            start={[0, 1.3]}
            end={[1.5, 0]}
          >
            <Image
              source={imageHV}
              style={{
                flex: 1,
                width: 180,
                height: null,
                resizeMode: 'contain',
              }}
            />
          </LinearGradient>
        </View>
      ),
    });
  }, [navigation]);
};

export default useHeader;
