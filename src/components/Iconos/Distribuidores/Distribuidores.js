import * as React from 'react';
import Svg, { Path, Circle } from 'react-native-svg';
import PropTypes from 'prop-types';
/* SVGR has dropped some elements not supported by react-native-svg: title */

function Distribuidores(props) {
  return (
    <Svg
      id="prefix__Capa_1"
      data-name="Capa 1"
      viewBox="0 0 88.63 77.84"
      {...props}
    >
      <Circle className="prefix__cls-1" cx={51.54} cy={11.39} r={11.39} />
      <Circle className="prefix__cls-1" cx={70.91} cy={50.11} r={17.72} />
      <Circle className="prefix__cls-1" cx={32.68} cy={46.14} r={8.86} />
      <Circle className="prefix__cls-1" cx={8.86} cy={68.97} r={8.86} />
      <Path
        className="prefix__cls-1"
        d="M9.75 69.88l24.4-23.92 36.16 2.93.2-2.52-37.31-3.03L7.97 68.07l1.78 1.81z"
      />
      <Path
        className="prefix__cls-1"
        transform="rotate(-25.32 61.982 31.25)"
        d="M60.7 10.37h2.53v41.74H60.7z"
      />
    </Svg>
  );
}

Distribuidores.defaultProps = {
  height: 45,
  fill: '#fff',
};

Distribuidores.proptypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

export default Distribuidores;
