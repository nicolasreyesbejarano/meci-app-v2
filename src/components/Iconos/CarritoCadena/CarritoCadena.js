import * as React from "react"
import Svg, { Path } from "react-native-svg"
import PropTypes from 'prop-types';
/* SVGR has dropped some elements not supported by react-native-svg: title */

function IconoCarritoCadenas(props) {
  return (
    <Svg data-name="Capa 1" viewBox="0 0 88.96 74.52" {...props}>
      <Path
        d="M83.75 12.05a1.53 1.53 0 002.07.67A5.76 5.76 0 0084.07 1.9L71.76 0a1.54 1.54 0 00-1.71 1.07L65.77 15H1.54a1.52 1.52 0 00-1.25.65A1.56 1.56 0 00.08 17l8.66 25a1.56 1.56 0 001.24 1l45.1 6.58-3 9.73H23A7.56 7.56 0 1030.58 67a7.49 7.49 0 00-1.49-4.48h14.68A7.49 7.49 0 0042.28 67a7.56 7.56 0 1012.47-5.75L72.61 3.27l11 1.73a2.68 2.68 0 01.81 5 1.53 1.53 0 00-.67 2.05zM49.84 62.48A4.48 4.48 0 1145.37 67a4.49 4.49 0 014.47-4.52zm-26.84 0A4.48 4.48 0 1118.54 67 4.48 4.48 0 0123 62.48zm41.8-44.42L56 46.69l-44.65-6.52-7.64-22.11zM53.73 24.4l-4.62 15a1.54 1.54 0 01-1.47 1.09 1.76 1.76 0 01-.45-.07 1.54 1.54 0 01-1-1.93l4.61-15a1.55 1.55 0 011.93-1 1.54 1.54 0 011 1.91zm-15.6 13.37a1.53 1.53 0 01-1.47 1.09 1.47 1.47 0 01-.46-.07 1.54 1.54 0 01-1-1.92l4.13-13.38a1.54 1.54 0 112.94.91zm-11-1.6a1.54 1.54 0 01-1.47 1.09 1.41 1.41 0 01-.45-.07 1.54 1.54 0 01-1-1.93l3.63-11.77a1.54 1.54 0 112.94.91zm-10.66-2.62A1.53 1.53 0 0115 34.63a1.74 1.74 0 01-.45-.06 1.54 1.54 0 01-1-1.93l2.82-9.15a1.54 1.54 0 112.94.91z"
        fill="#fff"
      />
    </Svg>
  )
}

IconoCarritoCadenas.defaultProps = {
  height: 45,
  fill: '#fff',
};

IconoCarritoCadenas.proptypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

export default IconoCarritoCadenas
