import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import PropTypes from 'prop-types';
/* SVGR has dropped some elements not supported by react-native-svg: title */

function Mayoristas(props) {
  return (
    <Svg
      id="prefix__Capa_1"
      data-name="Capa 1"
      viewBox="0 0 88.63 85.16"
      {...props}
    >
      <Path
        className="prefix__cls-1"
        d="M38.42 69A1.55 1.55 0 0137 67.92L16.55 5.36 1.32 3.08A1.55 1.55 0 011.78 0L18 2.44a1.53 1.53 0 011.2 1.05L39.89 67a1.55 1.55 0 01-1 2 1.61 1.61 0 01-.47 0zM48.73 75a1.55 1.55 0 01-.48-3L86.6 59.44a1.55 1.55 0 011 3L49.21 74.89a1.59 1.59 0 01-.48.11zM81.27 58.67a1.55 1.55 0 01-1.47-1.07L69.62 26.38 34.93 37.69a1.55 1.55 0 11-1-2.94L70.14 23a1.55 1.55 0 012 1l10.6 32.64a1.53 1.53 0 01-1 2 1.34 1.34 0 01-.47.03z"
      />
      <Path
        className="prefix__cls-1"
        d="M62.43 27.88A1.56 1.56 0 0161 26.81L53.49 3.89l-27 8.82a1.55 1.55 0 11-1-2.94L54 .47a1.61 1.61 0 011.18.09 1.59 1.59 0 01.77.9l7.95 24.39a1.55 1.55 0 01-1 1.95 1.61 1.61 0 01-.47.08zM47.24 40.72a1.57 1.57 0 01-.7-.17 1.52 1.52 0 01-.77-.9l-2.1-6.44a1.54 1.54 0 112.94-1l1.62 5 11.84-3.86-1.62-5a1.55 1.55 0 112.94-1l2.1 6.43a1.54 1.54 0 01-1 2l-14.77 4.86a1.61 1.61 0 01-.48.08z"
      />
      <Path
        className="prefix__cls-1"
        d="M36.72 15.68a1.54 1.54 0 01-1.47-1.07l-1.54-4.7a1.55 1.55 0 012.95-1l1.05 3.23 7.84-2.56-1-3.22a1.55 1.55 0 112.94-1L49 10.13a1.54 1.54 0 01-1 1.95L37.2 15.6a1.66 1.66 0 01-.48.08zM41.16 85.16A9.28 9.28 0 1150 73a9.29 9.29 0 01-6 11.7 9.18 9.18 0 01-2.84.46zm0-15.46a6.41 6.41 0 00-1.92.3 6.19 6.19 0 107.8 4 6.19 6.19 0 00-5.87-4.3z"
      />
      <Path
        className="prefix__cls-1"
        transform="rotate(-18.06 67.272 59.366)"
        d="M64.87 57.83h4.85v3.09h-4.85z"
      />
      <Path
        className="prefix__cls-1"
        transform="rotate(-18.06 53.63 23.426)"
        d="M51.22 21.88h4.85v3.09h-4.85z"
      />
      <Path
        className="prefix__cls-1"
        transform="rotate(-18.06 73.63 57.312)"
        d="M71.21 55.76h4.85v3.09h-4.85z"
      />
    </Svg>
  );
}

Mayoristas.defaultProps = {
  height: 45,
  fill: '#fff',
};

Mayoristas.proptypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

export default Mayoristas;
