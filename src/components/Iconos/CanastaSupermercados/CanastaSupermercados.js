import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import PropTypes from 'prop-types';
/* SVGR has dropped some elements not supported by react-native-svg: title */

function CanastaSupermercados({ height, ...props }) {
  return (
    <Svg
      data-name="Capa 1"
      viewBox="0 0 88.63 87.18"
      {...props}
      height={height}
    >
      <Path
        d="M86.25 39.6H2.37a1.94 1.94 0 00-1.58.81 2 2 0 00-.26 1.76l14.8 43.69a2 2 0 001.85 1.32h54.27a1.94 1.94 0 001.84-1.32L88.1 42.17a1.94 1.94 0 00-1.85-2.57zM75.07 68.49h-6.28l3.1-11h6.92zm-10.32 0h-7.27l1.69-11h8.68zm-9.54 14.8l1.67-10.91h6.78L60.6 83.29zm-8.94 0V72.38h6.67l-1.67 10.91zm-8.91 0l-1.68-10.91h6.7v10.91zm-9.34 0L25 72.38h6.78l1.67 10.91zM9.81 57.44h6.93l3.09 11h-6.27zm11 0h8.68l1.69 11h-7.3zm6.54-13.95l1.54 10.06h-9.2l-2.82-10.06zm19 13.95h9l-1.69 11h-7.39zm-3.89 11h-7.37l-1.69-11h9zm3.89-14.94V43.49h11.1l-1.62 10.06zm-3.89 0H32.8l-1.54-10.01h11.12zm17.38 0l1.46-10.01h10.46l-2.82 10.06zM12.83 43.49l2.82 10.06H8.49l-3.4-10.06zm2 28.89h6.05L24 83.29h-5.43zm55.22 10.91h-5.41l3.06-10.91h6zm10.08-29.74H73l2.8-10.06h7.74zM0 31.63a2 2 0 012-1.95h41.51L72.62.57a1.94 1.94 0 012.75 2.75L49 29.68h37.68a1.95 1.95 0 110 3.89H2a1.94 1.94 0 01-2-1.94z"
        fill="#fff"
      />
    </Svg>
  );
}

CanastaSupermercados.defaultProps = {
  height: 30,
  fill: '#fff',
};

CanastaSupermercados.proptypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

export default CanastaSupermercados;
