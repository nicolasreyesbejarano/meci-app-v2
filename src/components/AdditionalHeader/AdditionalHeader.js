import React, { useContext, useEffect, useState } from 'react';
import { View, Image, Text, Animated } from 'react-native';
import imageMerchandising from '@images/logo-merchandising.png';
import { styles } from './styles';
import { LinearGradient } from 'expo-linear-gradient';

const AdditionalHeader = ({ title, logo }) => {
  const style = styles();
  const headerSize = new Animated.Value(0);
  const size = logo ? 0 : 60;
  useEffect(() => {
    Animated.timing(headerSize, {
      toValue: 120 - size,
      duration: 600,
      useNativeDriver: false,
    }).start();
  }, []);
  return (
    <Animated.View
      style={{
        width: '100%',
        height: headerSize,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        position: 'relative',
        overflow: 'hidden',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}
    >
      <LinearGradient
        style={{
          flexGrow: 1,
          height: '100%',
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
        }}
        colors={['#0081ab', '#5ca664']}
        start={[0, 1.3]}
        end={[1.5, 0]}
      />
      <Text style={{ fontSize: 22, color: '#FFF', fontWeight: 'bold' }}>
        {title}
      </Text>
      {logo && (
        <View style={{ ...style.footer, marginTop: 30, marginBottom: 30 }}>
          <View style={style.meciImageHeader}>
            <Image source={imageMerchandising} style={style.imageHV} />
          </View>
        </View>
      )}
    </Animated.View>
  );
};

export default AdditionalHeader;
