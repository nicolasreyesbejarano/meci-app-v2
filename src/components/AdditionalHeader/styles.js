import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

export const styles = () =>
  StyleSheet.create({
    imageHV: {
      flex: 1,
      width: null,
      height: null,
      resizeMode: 'contain',
    },
    background: {
      flex: 1,
      width: '100%',
      height: 400,
      resizeMode: 'cover',
    },
    meciImage: {
      width: '100%',
      height: 40,
    },
    meciImageHeader: {
      width: '100%',
      height: 30,
    },
    footer: {
      width: '100%',
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
