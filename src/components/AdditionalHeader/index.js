import AdditionalHeader from './AdditionalHeader';

AdditionalHeader.defaultProps = {
  logo: true,
};

export default AdditionalHeader;
