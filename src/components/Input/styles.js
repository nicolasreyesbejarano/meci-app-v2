import styled from 'styled-components/native';

export const InputContainer = styled.TextInput`
  background-color: ${(props) => props.theme.white01};
  color: ${(props) => props.theme.gray01};
  font-size: 20px;
  width: 100%;
  height: 50px;
  border-radius: 50px;
  padding: 10px 25px;
`;
