import Input from './Input';
Input.defaultProps = {
  value: '',
  onChangeText: () => {},
  placeholder: '',
};

export default Input;
