import React, {useEffect, useState} from 'react';
import { View, StyleSheet, Text, Image, Dimensions, TouchableHighlight } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { InteractionManager, ActivityIndicator } from 'react-native';
import * as FileSystem from 'expo-file-system';
import DeepZoom from 'expo-image-deep-zoom';
import * as ImageManipulator from "expo-image-manipulator";

const tmpPath = `${FileSystem.documentDirectory}tmp`;
const CarouselItem = ({ item, onPress, index }) => {
  const { width, height } = Dimensions.get('window');
  const insets = useSafeAreaInsets();
  const getDimensions = () => {
    let toHeight = height;
    let toWidth = width;
    if (width < height) {
      toHeight = width;
      toWidth = height;
    }
    return { height: toHeight, width: toWidth };
  };
  const style = styles(insets, getDimensions());
  return (
    <TouchableHighlight
      underlayColor="transparent"
      style={style.cardView}
      onPress={()=>onPress(index)}
    >
      <Image style={style.image} source={{ uri: item.uri }} />
      {/* <View style={style.textView}>
        <Text style={style.itemTitle}>{item.title}</Text>
        <Text style={style.itemDescription}>{item.description}</Text>
      </View> */}
    </TouchableHighlight>
  );
};

const styles = (insets, { height, width }) =>
  StyleSheet.create({
    cardView: {
      flex: 1,
      width: width - insets.left - insets.right - 50,
      maxWidth: '100%',
      height: height,
      maxHeight: '92%',
      margin: 10,
      borderRadius: 10,
    },
    textView: {
      position: 'absolute',
      bottom: 10,
      margin: 10,
      left: 5,
    },
    image: {
      flex: 1,
      width: null,
      height: null,
      resizeMode: 'contain',
      borderRadius: 10,
    },
    itemTitle: {
      color: 'white',
      fontSize: 20,
      shadowColor: '#000',
      shadowOffset: { width: 0.8, height: 0.8 },
      shadowOpacity: 1,
      shadowRadius: 3,
      marginBottom: 5,
      fontWeight: 'bold',
      elevation: 5,
    },
    itemDescription: {
      color: 'white',
      fontSize: 12,
      shadowColor: '#000',
      shadowOffset: { width: 0.8, height: 0.8 },
      shadowOpacity: 1,
      shadowRadius: 3,
      elevation: 5,
    },
  });

export default CarouselItem;
