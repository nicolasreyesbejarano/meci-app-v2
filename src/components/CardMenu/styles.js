import styled from 'styled-components/native';

export const CardContainer = styled.TouchableHighlight.attrs((props) => ({
  underlayColor: props.theme.blue02,
}))`
  width: 100%;
  height: 100px;
  border-radius: 25px;
  background-color: ${(props) => props.theme.blue01};
`;
export const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: ${(props) => props.theme.white01};
  text-align: center;
  top: 35%;
`;
