import React from 'react';
import { ImageBackground } from 'react-native';
import { CardContainer, Title } from './styles';

const CardMenu = ({ style, title, background, ...props }) => {
  return (
    <CardContainer style={{ ...style, overflow: 'hidden' }} {...props}>
      <ImageBackground
        source={background}
        style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0 }}
      >
        <Title>{title}</Title>
      </ImageBackground>
    </CardContainer>
  );
};

export default CardMenu;
