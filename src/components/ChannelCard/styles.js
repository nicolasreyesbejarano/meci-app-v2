import styled from 'styled-components/native';

export const CardContainer = styled.TouchableHighlight.attrs((props) => ({
  underlayColor: props.theme.blue02,
}))`
  width: ${(props) => props.width || '100px'};
  height: ${(props) => props.height || '80px'};
  border-radius: 20px;
  display: flex;
  background-color: ${(props) => props.color};
  padding: 10px;
  justify-content: center;
  /* align-items: center; */
`;
export const Col = styled.View`
  flex-direction: column;
  justify-content: center;
  flex-grow: 1;
  background-color: blue;
`;
export const Title = styled.Text`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: ${(props) => props.theme.white01};
`;
