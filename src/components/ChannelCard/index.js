import ChannelCard from './ChannelCard';

ChannelCard.defaultProps = {
  title: 'Card Title',
  color: 'blue01',
  Icon: () => null,
};

export default ChannelCard;
