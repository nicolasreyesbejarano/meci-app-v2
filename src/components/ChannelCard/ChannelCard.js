import React from 'react';
import { CardContainer, Title, Col } from './styles';

const ChannelCard = ({ style, title, Icon, ...props }) => {
  return (
    <CardContainer style={{ ...style, overflow: 'hidden' }} {...props}>
      <>
        <Icon height={25} color={'#fff'} style={{ marginBottom: 5 }} />
        <Title>{title}</Title>
      </>
    </CardContainer>
  );
};

export default ChannelCard;
