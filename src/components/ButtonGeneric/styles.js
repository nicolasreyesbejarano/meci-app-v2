import styled from 'styled-components/native';

export const ButtonContainer = styled.TouchableHighlight.attrs((props) => ({
  underlayColor: props.invert ? props.theme.blue02 : props.theme.white02,
}))`
  padding: 12px 20px;
  width: ${(props) => props.width || '100%'};
  background-color: ${(props) =>
    props.invert ? props.theme.blue01 : props.theme.white01};
  border-radius: 50px;
  display: flex;
  align-items: center;
`;
export const ButtonText = styled.Text`
  color: ${(props) =>
    props.invert ? props.theme.white01 : props.theme.blue01};
  font-size: 20px;
  font-weight: bold;
`;
