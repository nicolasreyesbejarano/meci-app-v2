import ButtonGeneric from './ButtonGeneric';

ButtonGeneric.defaultProps = {
  title: 'Comenzar',
  invert: false,
  onPress: () => {},
  disabled: false,
  loading: false,
};

export default ButtonGeneric;
