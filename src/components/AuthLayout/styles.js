import styled from 'styled-components/native';
import { StyleSheet } from 'react-native';

export const LayoutContainer = styled.View`
  flex: 1;
  padding-top: ${(props) => props.insets.top}px;
  padding-bottom: ${(props) => props.insets.bottom}px;
  background-color: ${(props) => props.theme.secondBackground};
`;
export const LayoutHeader = styled.View`
  width: 100%;
  height: 50px;
  justify-content: center;
  padding-left: 15px;
  margin-bottom: 20px;
`;
