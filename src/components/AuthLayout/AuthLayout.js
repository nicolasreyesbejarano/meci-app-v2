import React from 'react';
import { StatusBar } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { LayoutContainer, LayoutHeader } from './styles';

const AuthLayout = ({ onPressArrow, children }) => {
  const insets = useSafeAreaInsets();
  return (
    <LayoutContainer insets={insets}>
      <StatusBar barStyle="dark-content" />
      <LayoutHeader>
        <Ionicons
          name="ios-arrow-back"
          size={35}
          color="#3e3e3e"
          onPress={onPressArrow}
        />
      </LayoutHeader>
      {children}
    </LayoutContainer>
  );
};

export default AuthLayout;
