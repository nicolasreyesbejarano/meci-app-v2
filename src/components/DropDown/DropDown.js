import React from 'react';
import DropDownPicker from 'react-native-dropdown-picker';

const FieldSelect = ({
  label,
  data,
  zIndex,
  defaultValue,
  dropDownHeight,
  onChangeItem,
  ...props
}) => {
  return (
    <DropDownPicker
      zIndex={zIndex}
      items={data}
      defaultValue={defaultValue}
      containerStyle={{
        height: 50,
        width: '100%',
      }}
      style={{
        backgroundColor: '#FFF',
        borderWidth: 0,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
      }}
      labelStyle={{
        // fontSize: 18,
        marginLeft: 0,
        color: '#3e3e3e',
        textAlign: 'center',
      }}
      itemStyle={{
        // fontSize: 18,
      }}
      dropDownStyle={{
        borderWidth: 0,
        position: 'absolute',
        zIndex: zIndex,
        height: dropDownHeight,
        borderRadius: 25,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
      }}
      
      onChangeItem={onChangeItem}
      {...props}
    />
    
  );
};
export default FieldSelect;
