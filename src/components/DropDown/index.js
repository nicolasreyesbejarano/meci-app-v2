import PropTypes from 'prop-types';
import DropDown from './DropDown';

DropDown.defaultProps = {
  label: '',
  data: [
    { label: 'Harinas de Trigo', value: 'uk' },
    { label: 'France', value: 'france' },
  ],
  zIndex: 0,
  dropDownHeight: 150,
  defaultValue: 'uk',
  onChangeItem: () => {},
};
DropDown.propTypes = {
  defaultValue: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  zIndex: PropTypes.number.isRequired,
  dropDownHeight: PropTypes.number.isRequired,
  onChangeItem: PropTypes.func.isRequired,
};
export default DropDown;
