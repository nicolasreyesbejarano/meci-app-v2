import React, { useCallback } from 'react';
import SearchBar from '../../containers/SearchBar';
import { SearchContainer } from './styles';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import * as ScreenOrientation from 'expo-screen-orientation';
import { useFocusEffect } from '@react-navigation/native';
const Search = () => {
  const insets = useSafeAreaInsets();
  useFocusEffect(
    useCallback(() => {
      changeScreenOriginal();
    }, [])
  );
  async function changeScreenOriginal() {
    await ScreenOrientation.lockAsync(
      ScreenOrientation.OrientationLock.PORTRAIT_UP
    );
  }
  return (
    <SearchContainer
      style={{ paddingTop: insets.top, paddingBottom: insets.bottom }}
    >
      <SearchBar />
    </SearchContainer>
  );
};

export default Search;
