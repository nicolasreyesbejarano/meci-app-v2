import styled from 'styled-components/native';

export const SearchContainer = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.secondBackground};
  padding-top: 0;
`;
