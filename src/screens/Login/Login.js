import React from 'react';
import AuthLayout from '../../components/AuthLayout';
import LoginForm from '../../containers/LoginForm';
import { useNavigation } from '@react-navigation/native';
import {
  LoginContainer,
  TitleContainer,
  FormContainer,
  FooterContainer,
  Title,
  Text,
} from './styles';

const Login = () => {
  const navigation = useNavigation();
  return (
    <AuthLayout onPressArrow={() => navigation.goBack()}>
      <LoginContainer>
        <TitleContainer>
          <Title>Iniciar sesión</Title>
          <Title>de tu cuenta</Title>
        </TitleContainer>
        <FormContainer>
          <LoginForm />
        </FormContainer>
        <FooterContainer>
          <Text>¿Olvidaste tu contraseña?</Text>
          <Text
            style={{ color: '#07a8ca' }}
            onPress={() => navigation.navigate('RecoverPassword')}
          >
            Recordar contraseña
          </Text>
        </FooterContainer>
      </LoginContainer>
    </AuthLayout>
  );
};

export default Login;
