import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import ButtonGeneric from '../../components/ButtonGeneric';
import { STATIC_DESCRIPTION, STATIC_DESCRIPTION_SECOND } from './utils/static';
import {
  ManualContainer,
  ContentInformation,
  ContentHeader,
  ContentButton,
  Text,
  Title,
  SubTitle,
  styles,
} from './styles';

const Manual = () => {
  const style = styles();
  const navigation = useNavigation();
  return (
    <ManualContainer>
      <ContentHeader style={{ overflow: 'hidden', paddingHorizontal: 10 }}>
        <LinearGradient
          colors={['#0081ab', '#5ca664']}
          start={[0, 1.3]}
          end={[1.5, 0]}
          style={style.linerGradient}
        />
        <Title>Manual de Ejecución</Title>
        <Title>Comercial Impecable</Title>
        <SubTitle>En el punto de venta</SubTitle>
      </ContentHeader>
      <ContentInformation>
        <Text>{STATIC_DESCRIPTION}</Text>
        <Text style={{ marginTop: 15, marginBottom: 15 }}>
          {STATIC_DESCRIPTION_SECOND}
        </Text>
        <ContentButton>
          <ButtonGeneric
            width="100%"
            title="Iniciar sesión"
            invert
            onPress={() => navigation.navigate('Login')}
          />
        </ContentButton>
      </ContentInformation>
    </ManualContainer>
  );
};

export default Manual;
