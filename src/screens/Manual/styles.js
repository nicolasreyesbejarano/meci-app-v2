import styled from 'styled-components/native';
import { StyleSheet } from 'react-native';

export const ManualContainer = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.background};
`;
export const ContentInformation = styled.View`
  flex: 2;
  background-color: ${(props) => props.theme.background};
  justify-content: flex-start;
  align-items: center;
  padding: 25px 40px;
`;
export const ContentHeader = styled.View`
  flex: 1.1;
  position: relative;
  background-color: transparent;
  justify-content: center;
  align-items: center;
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
`;
export const ContentImage = styled.View`
  width: 60%;
  height: 50px;
  margin-bottom: 50px;
`;
export const ContentButton = styled.View`
  flex-grow: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const ContentImageMerci = styled.View`
  width: 60%;
  height: 50px;
  margin-bottom: 50px;
`;
export const Title = styled.Text`
  font-size: 30px;
  font-weight: bold;
  color: ${(props) => props.theme.white01};
`;
export const SubTitle = styled.Text`
  position: absolute;
  top: 80%;
  left: 15%;
  font-size: 20px;
  font-weight: bold;
  color: ${(props) => props.theme.white01};
`;
export const Text = styled.Text`
  font-size: 16px;
  text-align: justify;
  color: ${(props) => props.theme.blue01};
`;

export const styles = () =>
  StyleSheet.create({
    linerGradient: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      height: '100%',
    },
  });
