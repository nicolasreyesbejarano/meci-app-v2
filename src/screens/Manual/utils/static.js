export const STATIC_DESCRIPTION =
  'Esta es una herramienta que pretende profesionalizar al equipo de campo brindándoles guías sobre lo que se espera encontrar en los puntos de venta en términos de surtido, exhibición y ubicación, con el fin de estandarizar la visibilidad de las marcas en punto de venta y mejorar la rotación de nuestros productos.';

export const STATIC_DESCRIPTION_SECOND =
  'El Manual de Ejecución Comercial Impecable es de uso exclusivo de Harinera del Valle S.A., prohibiéndose su reproducción o difusión a terceros no autorizados.';
