import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './Home';
import Channels from './Channels';
import Categories from './Categories';

const Stack = createStackNavigator();

const HomeRouter = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="HeaderStackNavigation" component={Home} />
      <Stack.Screen name="Categories" component={Categories} />
      <Stack.Screen name="Channels" component={Channels} />
    </Stack.Navigator>
  );
};

export default HomeRouter;
