import React, { useCallback } from 'react';
import { View, Dimensions, Image } from 'react-native';
import SelectCategories from '../../containers/SelectCategories';
import AdditionalHeader from '../../components/AdditionalHeader';
import { useNavigation } from '@react-navigation/native';
import useHeader from '../../hooks/useHeader';
import { SubTitle, CategoriesContent, styles } from './styles';
import { useFocusEffect } from '@react-navigation/native';
import * as ScreenOrientation from 'expo-screen-orientation';

const Categories = () => {
  useNavigation();
  useHeader({ border: 0 });
  const style = styles();
  let height = Dimensions.get('window').height * 0.2;
  useFocusEffect(
    useCallback(() => {
      changeScreenOriginal();
    }, [])
  );
  async function changeScreenOriginal() {
    await ScreenOrientation.lockAsync(
      ScreenOrientation.OrientationLock.PORTRAIT_UP
    );
  }
  return (
    <View style={{ flex: 1, paddingTop: 0 }}>
      <AdditionalHeader title="Categorías HV" logo={false}/>
          <SelectCategories 
            Header={()=>(
              <View style={{
                width:'100%',
                justifyContent:'center',
                alignItems:'center'
              }}>
                <SubTitle>Selecciona la Categoría</SubTitle>
              </View>
            )}
          />
    </View>
  );
};

export default Categories;
