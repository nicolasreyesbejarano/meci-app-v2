import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

export const SubTitle = styled.Text`
  font-size: 20px;
  margin-bottom: 20px;
  font-weight: bold;
  text-align: justify;
  color: ${(props) => props.theme.gray01};
`;
export const Row = styled.FlatList`
  flex-grow: 1;
  padding: 15px;
  padding-bottom: 10px;
  padding-top: 10px;
  flex-direction: row;
  justify-content: space-around;
`;
export const Col = styled.View`
  flex: 1;
  justify-content: space-around;
  align-items: center;
`;
export const LoadingContainer = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const CategoriesContent = styled.View`
  width: 100%;
  flex-grow: 1;
  background-color: ${(props) => props.theme.secondBackground};
  justify-content: flex-start;
  align-items: center;
  padding: 20px;
  position: relative;
`;
export const styles = () =>
  StyleSheet.create({
    imageHV: {
      flex: 1,
      width: null,
      height: null,
      resizeMode: 'contain',
    },
    background: {
      flex: 1,
      width: '100%',
      height: 400,
      resizeMode: 'cover',
    },
    meciImage: {
      width: '100%',
      height: 35,
    },
    tmImage: {
      width: '100%',
      height: 55,
      marginTop: 20,
      marginBottom:20
    },
    meciImageHeader: {
      width: '100%',
      height: 30,
    },
    footer: {
      width: '100%',
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
