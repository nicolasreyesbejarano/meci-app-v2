import React, { useCallback, useEffect } from 'react';
import { View, Dimensions, Image, ActivityIndicator } from 'react-native';
import imageMerchandising from '../../../assets/images/logo-merchandising-gris.png'; 
import contactChannels from '../../../assets/images/canales-de-contacto.png';
import { useNavigation } from '@react-navigation/native';
import useHeader from '../../hooks/useHeader';
import CardMenu from '../../components/CardMenu';
import { styles, LoadingContainer, SubTitle } from './styles';
import * as ScreenOrientation from 'expo-screen-orientation';
import { useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { requestHome, selectHome } from '../../redux/ducks/home.duck';
import { setCategory } from '../../redux/ducks/category.duck';

const Home = () => {
  const { fetching, data } = useSelector((state) => state.home);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const header = useHeader({ border: 25, back: false });
  const style = styles();
  useEffect(() => {
    dispatch(requestHome());
  }, []);
  useFocusEffect(
    useCallback(() => {
      changeScreenOriginal();
    }, [])
  );
  async function changeScreenOriginal() {
    await ScreenOrientation.lockAsync(
      ScreenOrientation.OrientationLock.PORTRAIT_UP
    );
  }
  let height = (Dimensions.get('window').height * 0.64) / 4.4;
  if (fetching)
    return (
      <LoadingContainer>
        <ActivityIndicator size="small" color={'#07a8ca'} />
        <SubTitle>Cargando informacion...</SubTitle>
      </LoadingContainer>
    );
  if (data.length === 0 || data.hasOwnProperty('error') || !data)
    return (
      <LoadingContainer>
        <SubTitle>Al parecer no hay</SubTitle>
        <SubTitle>informacion disponible</SubTitle>
      </LoadingContainer>
    );
  const navigationData = [
    'Channels',
    'CategoriesRender',
    'Categories',
    'CategoriesRender',
  ];
  return (
    <View style={{ padding: 25, flex: 1 }}>
      {data?.map((section, index) => (
        <CardMenu
          key={section.Seccion}
          style={{ marginBottom: 10, height: height }}
          title={section.nombre}
          background={contactChannels}
          // background={section.imgRepresentativa}
          onPress={() => {
            if (navigationData[index] === 'CategoriesRender') {
              dispatch(setCategory(section));
            } else {
              dispatch(selectHome(section));
            }
            setTimeout(() => {
              navigation.navigate(navigationData[index]);
            }, 100);
          }}
        />
      ))}
      <View style={style.footer}>
        <View style={style.meciImage}>
          <Image source={imageMerchandising} style={style.imageHV} />
        </View>
      </View>
    </View>
  );
};

export default Home;
