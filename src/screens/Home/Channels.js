import React, { useCallback } from "react";
import { View, Dimensions, Image, FlatList } from "react-native";
import logoTm from "../../../assets/images/logo-tm.png";
import ChannelCard from "../../components/ChannelCard";
import AdditionalHeader from "../../components/AdditionalHeader";
import CanastaSupermercados from "../../components/Iconos/CanastaSupermercados/CanastaSupermercados";
import CarritoCadena from "../../components/Iconos/CarritoCadena/CarritoCadena";
import Distribuidores from "../../components/Iconos/Distribuidores/Distribuidores";
import Mayoristas from "../../components/Iconos/Mayoristas/Mayoristas";
import Merchandising from "../../components/Iconos/Merchandising/Merchandising";
import Minimercados from "../../components/Iconos/Minimercados/Minimercados";
import TiendasPanaderias from "../../components/Iconos/TiendasPanaderias/TiendasPanaderias";
import { useNavigation } from "@react-navigation/native";
import useHeader from "../../hooks/useHeader";
import { styles } from "./styles";
import { useDispatch, useSelector } from "react-redux";
import { setCategory } from "../../redux/ducks/category.duck";
import { useFocusEffect } from "@react-navigation/native";
import * as ScreenOrientation from "expo-screen-orientation";

const REMOVE_ITEM = "MERCHANDISING";
const MERGE_DATA = [
  CarritoCadena,
  CarritoCadena,
  Mayoristas,
  TiendasPanaderias,
  CanastaSupermercados,
  Minimercados,
  Distribuidores,
  Merchandising,
];
const Channels = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  useHeader({ border: 0 });
  const style = styles();
  let height = 80;
  let size = { height: `${height}px`, width: `100%` };
  useFocusEffect(
    useCallback(() => {
      // changeScreenOrientation();
      changeScreenOriginal();
    }, [])
  );
  async function changeScreenOriginal() {
    await ScreenOrientation.lockAsync(
      ScreenOrientation.OrientationLock.PORTRAIT_UP
    );
  }
  const { dataSelected } = useSelector((state) => state.home);
  const goToExplore = (section) => {
    const image = { ...section, imagenesSeccion: section.img };
    delete image.img;
    dispatch(setCategory(image));
    setTimeout(() => {
      navigation.navigate("CategoriesRender");
    }, 100);
  };
  const renderItem = ({ item, index }) => {
    if (item.nombre.toUpperCase() === REMOVE_ITEM) {
      return null;
    }
    return (
      <ChannelCard
        {...size}
        title={item.nombre}
        color={item.color}
        Icon={MERGE_DATA[index]}
        onPress={() => goToExplore(item)}
      />
    );
  };
  return (
    <View style={{ flex: 1, paddingTop: 0 }}>
      <AdditionalHeader title="Descripción de los Canales" logo={false} />
      <FlatList
        contentContainerStyle={{
          paddingVertical: 20,
        }}
        style={{
          paddingHorizontal:15
        }}
        data={dataSelected.caneles}
        ListHeaderComponent={() => (
          <View style={style.tmImage}>
            <Image source={logoTm} style={style.imageHV} />
          </View>
        )}
        ItemSeparatorComponent={({ highlighted }) => (
          <View style={{ marginBottom: 10 }} />
        )}
        renderItem={renderItem}
        keyExtractor={(item) => item.nombre}
      />
    </View>
  );
};

export default Channels;
