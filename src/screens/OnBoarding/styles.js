import styled from 'styled-components/native';
import { StyleSheet } from 'react-native';

export const OnBoardingContainer = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.background};
`;
export const ContentInformation = styled.View`
  flex: 2;
  background-color: ${(props) => props.theme.background};
`;
export const ContentFooter = styled.View`
  flex: 1.2;
  background-color: transparent;
  justify-content: center;
  align-items: center;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
`;
export const ContentImage = styled.View`
  width: 60%;
  height: 50px;
  margin-bottom: 50px;
`;

export const ContentImageMerci = styled.View`
  width: 60%;
  height: 50px;
  margin-bottom: 50px;
`;

export const styles = () =>
  StyleSheet.create({
    linerGradient: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      height: '100%',
    },
    imageHV: {
      flex: 1,
      width: null,
      height: null,
      resizeMode: 'contain',
    },
    meciContainer: {
      flex: 2,
      justifyContent: 'flex-end',
      paddingBottom: 50,
    },
    meciImage: {
      width: '100%',
      height: 130,
    },
    merchContainer: {
      flex: 0.8,
      justifyContent: 'center',
    },
    merchImage: {
      width: '100%',
      height: 50,
    },
  });
