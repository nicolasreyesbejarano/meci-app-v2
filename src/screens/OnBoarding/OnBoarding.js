import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import ButtonGeneric from '../../components/ButtonGeneric';
import { useNavigation } from '@react-navigation/native';
import imageHV from '../../../assets/images/logo-hv.png';
import imageMerci from '../../../assets/images/logo-meci.png';
import imageMerchandising from '../../../assets/images/logo-merchandising.png';
import { Image, View } from 'react-native';
import {
  OnBoardingContainer,
  ContentInformation,
  ContentFooter,
  ContentImage,
  styles,
} from './styles';

const OnBoarding = () => {
  const style = styles();
  const navigation = useNavigation();
  return (
    <OnBoardingContainer>
      <ContentInformation>
        <View style={style.meciContainer}>
          <View style={style.meciImage}>
            <Image source={imageMerci} style={style.imageHV} />
          </View>
        </View>
        <View style={style.merchContainer}>
          <View style={style.merchImage}>
            <Image source={imageMerchandising} style={style.imageHV} />
          </View>
        </View>
      </ContentInformation>
      <ContentFooter style={{ overflow: 'hidden' }}>
        <LinearGradient
          colors={['#0081ab', '#5ca664']}
          start={[0, 1.3]}
          end={[1.5, 0]}
          style={style.linerGradient}
        />
        <ContentImage>
          <Image source={imageHV} style={style.imageHV} />
        </ContentImage>
        <ButtonGeneric
          width="90%"
          onPress={() => navigation.navigate('Manual')}
        />
      </ContentFooter>
    </OnBoardingContainer>
  );
};

export default OnBoarding;
