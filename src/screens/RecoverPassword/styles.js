import styled from 'styled-components/native';

export const RecoverPasswordContainer = styled.View`
  flex: 1;
  padding: 10px 40px;
`;
export const TitleContainer = styled.View`
  width: 100%;
  margin-bottom: 50px;
`;
export const FormContainer = styled.View`
  width: 100%;
`;
export const FooterContainer = styled.View`
  width: 100%;
  flex-grow: 1;
  justify-content: flex-end;
  align-items: center;
`;
export const Title = styled.Text`
  font-size: 30px;
  font-weight: bold;
  color: ${(props) => props.theme.blue01};
`;
export const SubTitle = styled.Text`
  position: absolute;
  top: 80%;
  left: 15%;
  font-size: 20px;
  font-weight: bold;
  color: ${(props) => props.theme.white01};
`;
export const Text = styled.Text`
  font-size: 20px;
  opacity: 0.4;
  text-align: justify;
  color: ${(props) => props.theme.gray01};
`;
