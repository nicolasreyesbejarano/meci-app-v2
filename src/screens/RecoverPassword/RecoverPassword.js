import React from 'react';
import AuthLayout from '../../components/AuthLayout';
import { useNavigation } from '@react-navigation/native';
import RecoverPasswordForm from '../../containers/RecoverPasswordForm';
import {
  RecoverPasswordContainer,
  FooterContainer,
  TitleContainer,
  FormContainer,
  Title,
  Text,
} from './styles';

const RecoverPassword = () => {
  const navigation = useNavigation();
  return (
    <AuthLayout onPressArrow={() => navigation.goBack()}>
      <RecoverPasswordContainer>
        <TitleContainer>
          <Title>Recuperar mi</Title>
          <Title>Contraseña</Title>
        </TitleContainer>
        <FormContainer>
          <RecoverPasswordForm onSuccess={() => navigation.goBack()} />
        </FormContainer>
        <FooterContainer>
          <Text>Recuerda revisar tu correo</Text>
          <Text style={{ color: '#07a8ca' }}>Despues de la solicitud</Text>
        </FooterContainer>
      </RecoverPasswordContainer>
    </AuthLayout>
  );
};

export default RecoverPassword;
