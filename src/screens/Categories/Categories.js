import React, { useCallback, useState } from 'react';
import { View, Image, Dimensions } from 'react-native';
import imageHV from '../../../assets/images/logo-hv.png';
import imageMerchandising from '../../../assets/images/logo-merchandising.png';
import { useNavigation } from '@react-navigation/native';
import * as ScreenOrientation from 'expo-screen-orientation';
import { useFocusEffect } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Ionicons } from '@expo/vector-icons';
import Carousel from '../../components/Carousel';
import { styles, Title } from './styles';
import { useSelector } from 'react-redux';
import { LinearGradient } from 'expo-linear-gradient';
import ImageViewer from 'react-native-image-zoom-viewer';
import ButtonGeneric from '../../components/ButtonGeneric';

const ICON_SPACE = 35;
const { width, height } = Dimensions.get('window');
const Categories = () => {
  useFocusEffect(
    useCallback(() => {
      changeScreenOrientation();
      //return () => changeScreenOriginal();
    }, [])
  );
  const [ visible, setVisible ] = useState(false);
  const [ toZoomInit, setToZoomInit ] = useState(null);
  async function changeScreenOrientation() {
    await ScreenOrientation.lockAsync(
      ScreenOrientation.OrientationLock.LANDSCAPE_LEFT
    );
  }
  async function changeScreenOriginal() {
    await ScreenOrientation.lockAsync(
      ScreenOrientation.OrientationLock.PORTRAIT_UP
    );
  }
  const handleData = (category) => {
    const newArray = category.data.imagenesSeccion?.map((image) => ({
      uri: `http://54.71.36.162/imagenes/${image}`,
      title: 'Empty',
      description: 'Empty',
    }));
    let dataSend = { ...category.data, imagenesSeccion: newArray };
    return { ...category, data: dataSend };
  };
  const { data: dataInfo } = useSelector((state) => handleData(state.category));
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const style = styles();
  const getGradientColor = () => {
    const { color } = dataInfo;
    let long = color.length;
    let arrayColor = [...color];
    if (long == 1) {
      arrayColor = [arrayColor[0], arrayColor[0]];
    }
    return arrayColor;
  };
  const handleImagesZoom = (dataImage) => {
    return dataImage.map((item)=>{
      return{ url: item.uri}})
    
  }
  const readyToZoom = () => (visible && toZoomInit !=null);
  return (
    <>
      {readyToZoom() && (
        <View
          style={{
            position: 'absolute',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            width: width > height ? width : height,
            zIndex: 99,
            height: width > height ? height - 25 : width - 25,
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            fontSize: 0,
          }}
        >
          <ButtonGeneric
            title="Cerrar"
            invert
            onPress={() => {
              setToZoomInit(null)
              setVisible(false)
            }}
          />
          <ImageViewer
            style={{ width: '100%' }}
            imageUrls={handleImagesZoom(dataInfo.imagenesSeccion)}
            backgroundColor="white"
            index={toZoomInit}
            onCancel={() => {
              console.log('closeing');
            }}
          />
        </View>
      )}
      <View
        style={{
          padding: insets.left !== 0 ? insets.left : 15,
          paddingTop: 0,
          backgroundColor: '#FFF',
          flex: 1,
        }}
      >
        <View
          style={{
            width: width > height ? width - 20 : height - 20,
            position: 'relative',
            height: 25 + ICON_SPACE,
            paddingHorizontal: ICON_SPACE,
            zIndex: 10,
          }}
        >
          <View style={{ position: 'absolute', top: 15 }}>
            <Ionicons
              name="ios-arrow-back"
              color="#3e3e3e"
              size={ICON_SPACE}
              onPress={() => navigation.goBack()}
              style={{ paddingVertical: 0, paddingHorizontal: 10 }}
            />
          </View>
          <View
            style={{
              borderBottomLeftRadius: 35,
              borderBottomRightRadius: 35,
              overflow: 'hidden',
            }}
          >
            <LinearGradient
              colors={getGradientColor() || ['#0081ab', '#5ca664']}
              start={[0, 1.3]}
              end={[1.5, 0]}
              style={style.header}
            >
              <Title>{dataInfo.nombre || 'Seccion'}</Title>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={{
                    height: 45,
                    width: 100,
                    paddingRight: 10,
                    borderRightWidth: 0.5,
                    borderColor: '#FFF',
                  }}
                >
                  <Image source={imageHV} style={style.imageHV} />
                </View>
                <View
                  style={{
                    height: 45,
                    width: 100,
                    paddingLeft: 10,
                  }}
                >
                  <Image source={imageMerchandising} style={style.imageHV} />
                </View>
              </View>
            </LinearGradient>
          </View>
        </View>
        <View
          style={{
            flexGrow: 1,
            width: '100%',
          }}
        >
          <Carousel
            data={dataInfo.imagenesSeccion}
            onPress={(index) => {
              setToZoomInit(index)
              setVisible(true) 
            }}
            
          />
        </View>
      </View>
    </>
  );
};

export default Categories;
