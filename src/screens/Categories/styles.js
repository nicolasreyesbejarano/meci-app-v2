import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

export const Title = styled.Text`
  font-size: 30px;
  font-weight: bold;
  color: ${(props) => props.theme.white01};
`;

export const styles = () =>
  StyleSheet.create({
    imageHV: {
      flex: 1,
      width: null,
      height: null,
      resizeMode: 'contain',
    },
    meciImage: {
      width: '100%',
      height: 40,
    },
    header: {
      height: 70,
      width: '100%',
      borderBottomLeftRadius: 35,
      borderBottomRightRadius: 35,
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingHorizontal: 25,
      position: 'relative',
      flexDirection: 'row',
    },
  });
