import styled from 'styled-components/native';

export const ProfileContainer = styled.View`
  flex: 1;
  padding-top: ${(props) => props.insets.top}px;
  padding-bottom: ${(props) => props.insets.bottom}px;
  background-color: ${(props) => props.theme.secondBackground};
`;

export const UserContainer = styled.View`
  flex: 1;
  padding: 10px 40px;
`;
export const TitleContainer = styled.View`
  width: 100%;
  margin-bottom: 50px;
`;
export const Title = styled.Text`
  font-size: 30px;
  font-weight: bold;
  color: ${(props) => props.theme.blue01};
`;
