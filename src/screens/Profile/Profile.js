import React from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import UserData from '../../containers/UserData';
import {
  ProfileContainer,
  UserContainer,
  TitleContainer,
  Title,
} from './styles';

const Profile = () => {
  const insets = useSafeAreaInsets();
  return (
    <ProfileContainer insets={insets}>
      <UserContainer>
        <TitleContainer>
          <Title>Tu Cuenta</Title>
        </TitleContainer>
        <UserData />
      </UserContainer>
    </ProfileContainer>
  );
};

export default Profile;
